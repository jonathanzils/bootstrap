$(function(){
    $('.carousel').carousel({
        interval: 500
    });

    $('#slide').on('slid.bs.carousel', function(e){
        if (e.to == 1)
        {
            $('#slide').carousel('pause');          
        }
        else
        {
            $('#slide').carousel('cycle')
        }
    })

    $('.carousel-control-prev').click(function(){
        $('#slide').carousel('prev')
    });
    $('.carousel-control-next').click(function(){
        $('#slide').carousel('next');
    })

   $('li.dropdown').hover(function(){
        $('.dropdown-menu').stop(true,true).toggle('show');
    }),function(){
        $('.dropdown-menu').stop(true,true).toggle('hide');
    }

    $('.navbar-toggler').click(function(){
        var verificarClass = $(this).hasClass('collapsed');
        if (!verificarClass) {
            $('#changeUser').removeClass('show');
        }
    })
    $('input[name="molho"').change(function(){
        var molho = $(this).val()
        if (molho == 1) {
            $('.qtdMolho').removeClass('d-none')
        }
        else{
            $('.qtdMolho').addClass('d-none')
        }
    })
    $('.list-group').find('.active').each(function(){
        var itemActive = $(this).attr('aria-current');
        var nome = $(this).html()
        
        if(itemActive){
            console.log(nome)
        }         
    })

     //setTimeout(function(){
        // $('.modal').modal({
        //     backdrop:'static',
        //     keyboard: false
        // });
    //},2000);
    // $(function () {
    //     $('[data-toggle="popover"]').popover()
    //   })
    $('.toast').toast({
        delay: 3000
    })

    $('input.startToast').focus(function(){
         mensagem = $(this).attr('data-toast-msg');
         $('.toast').find('.toast-body').html(mensagem)
         $('.toast').find('.horaAtual').html(dataAtual());
        $('.toast').toast('show');
    })
    
    dataAtual = function(){
        var data =new Date();
        var hora = data.getHours();
        var min = (data.getMinutes() < 10 ? '0' + data.getMinutes() : data.getMinutes());

        return hora+'h'+min+'min';
    }
})